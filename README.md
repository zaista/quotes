# quotes

Game of quotes is a puzzling and challenging little game in which you have to guess a famous quote.

See the real thing in action here: https://quotes.jovanilic.com/quotes.php

Read LICENSE.md for license information.