<?php
    include 'config.php';
    $xml = get_config('config.xml');
    $mysqli = connect($xml);

    // validation flag
    $form_validation = true;
    // validation message
    $validation_message = "";
    // TODO BIG
    // see what needs updating
    $update_username = false;
    $update_email = false;

    // Check if password is correct
    if (isset($_POST['password'])) {
        $password = md5(mysqli_real_escape_string($mysqli, $_POST['password']));

        // Check if username exists in database
        $stmt = $mysqli->prepare('SELECT username FROM users WHERE username = ?');
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            $validation_message .= "Username already exsists! ";
        } else {
            $update_username = true;
        }
    }

    // Check if username is submitted
    if (isset($_POST['username'])) {
        $username = mysqli_real_escape_string($mysqli, $_POST['username']);

        // Check if username exists in database
        $stmt = $mysqli->prepare('SELECT username FROM users WHERE username = ?');
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            $validation_message .= "Username already exsists! ";
        } else {
            $update_username = true;
        }
    }

    // Check if email is submitted
    if (!isset($_POST['email'])) {
        $form_validation = false;
        $validation_message .= "Email not set! ";
    } else {
        $email = mysqli_real_escape_string($mysqli, $_POST['email']);
        // Check if email exists in database
        $stmt = $mysqli->prepare('SELECT email FROM users WHERE email = ?');
        $stmt->bind_param('s', $email);

        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            $form_validation = false;
            $validation_message .= "Email already exsists! ";
        }
    }

    // insert user or return raised errors
    if ($form_validation) {

        // insert user into database
        $stmt = $mysqli->prepare('INSERT INTO users (username, password, email, insert_date, active) VALUES (?, ?, ?, NOW(), 0)');
        $stmt->bind_param('sss', $username, $password, $email);

        $stmt->execute();
        $result = $stmt->affected_rows;

        if ($result === 1) {

            // user registered
            echo 'User successfully registered';
            exit;
        } else {
            echo "Error. Code 4";
            exit;
        }
    } else {
        echo $validation_message;
        exit;
    }
