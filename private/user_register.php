<?php
    include 'config.php';
    $xml = get_config('config.xml');
    $mysqli = connect($xml);

    // validation flag
    $form_validation = true;
    // validation message
    $validation_message = "";

    // Check if username is submitted
    if (!isset($_POST['username']) || $_POST['username'] == '') {
        $form_validation = false;
        $validation_message .= "Username cannot be empty.";
    } else {
        $username = mysqli_real_escape_string($mysqli, $_POST['username']);
        // Check if username exists in database
        $stmt = $mysqli->prepare('SELECT username FROM users WHERE username = ?');
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            $form_validation = false;
            $validation_message .= "Username already exsists. Please change it.";
        }
    }

    // Check if password is submitted
    if (!isset($_POST['password']) || $_POST['password'] == '') {
        $form_validation = false;
        $validation_message .= "Password cannot be empty.";
    } else {
        $password = md5(mysqli_real_escape_string($mysqli, $_POST['password']));
    }

    // Check if email is submitted
    if (!isset($_POST['email']) || $_POST['email'] == '') {
        $form_validation = false;
        $validation_message .= "Email cannot be empty.";
    } else {
        $email = mysqli_real_escape_string($mysqli, $_POST['email']);
        // Check if email exists in database
        $stmt = $mysqli->prepare('SELECT email FROM users WHERE email = ?');
        $stmt->bind_param('s', $email);

        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            $form_validation = false;
            $validation_message .= "Email already exsists. Please change or recover it.";
        }
    }

    // insert user or return raised errors
    if ($form_validation) {

        // insert user into database
        $stmt = $mysqli->prepare('INSERT INTO users (username, password, email, insert_date, active) VALUES (?, ?, ?, NOW(), 0)');
        $stmt->bind_param('sss', $username, $password, $email);

        $stmt->execute();
        $result = $stmt->affected_rows;

        if ($result === 1) {
            $user_id = $stmt->insert_id;

            // generate registration key
            $registration_key = rand(1111111111, 9999999999);

            // insert registration key into database
            $stmt = $mysqli->prepare('INSERT INTO registrations (registration_key, user_id, insert_date) VALUES (?, ?, NOW())');
            $stmt->bind_param('ii', $registration_key, $user_id);

            $stmt->execute();
            $result = $stmt->affected_rows;

            if ($result === 1) {

                //send verification email
                $email_subject = "User activation";
                $email_text = '
        Confirm your registration by following this link:
        https://quotes.jovanilic.com/activate.php?key=' . $registration_key . '

        Thank you for your registration,
        Game of Quotes';

                $email_headers = "From: quotes@jovanilic.com";

                // use wordwrap() if lines are longer than 70 characters
                $email_text = wordwrap($email_text, 70);

                // send email
                mail($email, $email_subject, $email_text, $email_headers);

                echo 'User successfully registered. Verification email sent.';
                exit;
            } else {
                echo "Error preparing user activation";
                exit;
            }
        } else {
            echo "Error adding user to database";
            exit;
        }
    } else {
        echo $validation_message;
        exit;
    }
