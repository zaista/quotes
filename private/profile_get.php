<?php
    include 'config.php';
    $xml = get_config('config.xml');
    $mysqli = connect($xml);

    // initialize variables so that errors don't appear for user
    $user_timeline = array();
    $solved_quotes_count = '';
    $uploaded_quotes_count = '';
    $max_quotes_per_day = '';

    $user_data = new stdClass();

    if (isset($_SESSION['userid']) && $_SESSION['userid'] != '') {
        $user_data->userid = $_SESSION['userid'];
        $user_data->username = $_SESSION['username'];
        $user_data->useremail = $_SESSION['useremail'];

        // get all solved quotes for user
        $stmt = $mysqli->prepare('
                SELECT IFNULL(solutions.insert_date, quotes.insert_date) as insert_date, quotes.quote as content, quotes.author as addition, quotes.id as link, IF(ISNULL(solutions.insert_date), \'upload\', \'quote\') as mark FROM quotes
                    LEFT JOIN solutions ON solutions.quote_id = quotes.id
                    WHERE solutions.user_id = ?
                    OR quotes.user_id = ?
                UNION
                SELECT insert_date, username, null, null, \'user\' FROM users
                    WHERE id = ?
                ORDER BY insert_date DESC;');
        $stmt->bind_param('iii', $_SESSION['userid'], $_SESSION['userid'], $_SESSION['userid']);

        $stmt->execute();
        $result = $stmt->get_result();


        while ($row = $result->fetch_assoc()) {
            $user_timeline[] = $row;
        }

        $user_data->timeline = $user_timeline;

        // get number of solved quotes and uploaded quotes
        $stmt = $mysqli->prepare('SELECT (SELECT count(id) FROM solutions WHERE user_id = ?) AS solved, (SELECT count(id) FROM quotes WHERE user_id = ?) AS uploaded');
        $stmt->bind_param('ii', $_SESSION['userid'], $_SESSION['userid']);
        $stmt->execute();

        $result = $stmt->get_result();
        $row = $result->fetch_assoc();

        $user_data->solved = $row['solved'];
        $user_data->uploaded = $row['uploaded'];

        // get maximum number of solved quotes in single day
        $stmt = $mysqli->prepare('SELECT MAX(days) as max FROM (
                                        SELECT count(insert_date) as days FROM solutions WHERE user_id = ?
                                        GROUP BY date(insert_date)) AS result');
        $stmt->bind_param('i', $_SESSION['userid']);
        $stmt->execute();

        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
        $user_data->max = $row['max'];

        echo json_encode($user_data);
    } else {
        $user_data->error = "User not logged in";
        echo json_encode($user_data);
    }