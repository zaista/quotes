<?php
    include 'config.php';
    $xml = get_config('config.xml');
    $mysqli = connect($xml);

    // validation flag
    $form_validation = true;

    // Check if username is submitted
    if (!isset($_POST['username']) || $_POST['username'] === '') {
        $form_validation = false;
    } else {
        $username = $_POST['username'];
    }

    // Check if password is submitted
    if (!isset($_POST['password']) || $_POST['password'] === '') {
        $form_validation = false;
    } else {
        $password = md5($_POST['password']);
    }

    // login user or return raised errors
    if ($form_validation) {

        // match username and password
        $stmt = $mysqli->prepare('SELECT id, email, active FROM users WHERE username = ? AND password = ?');
        $stmt->bind_param('ss', $username, $password);

        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();

            // check if user is registered
            $active = $row['active'];

            if ($active == 1) {
                $id = $row['id'];
                $email = $row['email'];

                // user logged in
                $_SESSION['userid'] = $id;
                $_SESSION['username'] = $username;
                $_SESSION['useremail'] = $email;

                echo 'User successfully logged in';
                exit;
            } else {
                echo "User not activated, click <a class=\"alert-link\" href=\"activate.php?username=" . $username . "\">here</a> to send an activation email";
                exit;
            }
        } else {
            echo "Wrong username or password! ";
            exit;
        }
    } else {
        echo 'Username and/or password not set';
        exit;
    }
