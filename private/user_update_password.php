<?php
    include 'config.php';
    $xml = get_config('config.xml');
    $mysqli = connect($xml);

    // validation flag
    $form_validation = true;
    // validation message
    $validation_message = "";

    // Check if oldpassword is correct
    if (!isset($_POST['oldpassword']) || $_POST['oldpassword'] == '') {
        $form_validation = false;
        $validation_message .= "Old password not set! ";
    } else {
        $oldpassword = md5($_POST['oldpassword']);

        // Check if old password is correct
        $userid = $_SESSION['userid'];
        $stmt = $mysqli->prepare('SELECT username FROM users WHERE id = ? AND password = ?');
        $stmt->bind_param('ss', $userid, $oldpassword);

        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows < 1) {
            $form_validation = false;
            $validation_message .= "Old password incorrect! ";
        }
    }

    // Check if new password is submitted
    if (!isset($_POST['newpassword']) || $_POST['newpassword'] == '') {
        $form_validation = false;
        $validation_message .= "New password not set! ";
    } else {
        $newpassword = md5($_POST['newpassword']);
    }

    // Check if repeat password is submitted
    if (!isset($_POST['repeatpassword']) || $_POST['repeatpassword'] == '') {
        $form_validation = false;
        $validation_message .= "Please repeat the password! ";
    } else {
        $repeatpassword = md5($_POST['repeatpassword']);

        if ($newpassword != $repeatpassword) {
            $form_validation = false;
            $validation_message .= "Passwords do not match! ";
        }
    }

    // update user password
    if ($form_validation) {
        $userid = $_SESSION['userid'];
        $stmt = $mysqli->prepare('UPDATE users SET password = ? WHERE id = ?');
        $stmt->bind_param('ss', $newpassword, $userid);

        $stmt->execute();
        $result = $stmt->affected_rows;

        if ($result > 0) {
            session_unset();
            // destroy the session
            session_destroy();

            echo "Password successfully updated!";
            exit;
        } else {
            echo "Password could not be updated! ";
            exit;
        }
    } else {
        echo $validation_message;
        exit;
    }
