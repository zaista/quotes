<?php
    include 'config.php';
    $xml = get_config('config.xml');
    $mysqli = connect($xml);

    // Check if email is submitted
    if (!isset($_POST['email']) || $_POST['email'] == '') {
        echo "Email cannot be empty.";
        exit;
    } else {
        $email = $_POST['email'];
        // Check if username exists in database
        $stmt = $mysqli->prepare('SELECT username FROM users WHERE email = ?');
        $stmt->bind_param('s', $email);

        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows == 1) {
            $row = $result->fetch_assoc();
            $username = $row['username'];
            $temp_password = rand(111111, 999999);
            $hashed_password = md5($temp_password);

            // insert registration key into database
            $stmt = $mysqli->prepare('UPDATE users SET password = ? WHERE username = ?');
            $stmt->bind_param('ss', $hashed_password, $username);

            $stmt->execute();
            $result = $stmt->affected_rows;

            if ($result === 1) {

                //send recover email
                $email_subject = "User password reset";
                $email_text = 'Password for user: ' . $username . ' has been reset. Login to your account using the following password:\n
                        ' . $temp_password . '
                        \n
                        As soon as you login, please change your password!
                        \n\n
                        Thank you,\n
                        Game of Qutoes';

                $email_headers = "From: quotes@jovanilic.com";

                // use wordwrap() if lines are longer than 70 characters
                $email_text = wordwrap($email_text, 70);

                // send email
                mail($email, $email_subject, $email_text, $email_headers);

                echo 'Email sent.';
                exit;
            } else {
                echo "Error sending activation mail.";
                exit;
            }
        } else {
            echo "Email doesn't exist. Please ask for the support at quotes@jovanilic.com";
            exit;
        }
    }
