<?php

    include 'config.php';
    $xml = get_config('config.xml');
    $mysqli = connect($xml);

    // validation flag
    $form_validation = true;

    $response = new stdClass();

    // check if userid is set
    if (isset($_SESSION['userid'])) {
        $response->loggedin = true;
        $userid = $_SESSION['userid'];
    } else {
        $response->loggedin = false;
    }

    // check if quote is submitted
    if (isset($_POST['quote'])) {
        $quote = $_POST['quote'];
    } else {
        $form_validation = false;
    }

    // check if author is submitted
    if (isset($_POST['author'])) {
        $author = $_POST['author'];
    } else {
        $form_validation = false;
    }

    if ($form_validation) {

        // perform the SQL query to insert a quote to database
        $sql = "SELECT id, quote FROM quotes";
        if (!$result = $mysqli->query($sql)) {
            $response->error = "Code 2";
    
            echo json_encode($response);
            exit;
        }

        // check if any result is returned
        if ($result->num_rows === 0) {
            $response->error = "Code 3";
    
            echo json_encode($response);
            exit;
        }

        // get the submitted quote for comparison
        $submitted_quote = preg_replace('/[^A-Za-z0-9\-]/', '', strtolower($quote));

        // compare submitted to database quotes
        $response->existed = false;
        while ($row = $result->fetch_assoc()) {
            $database_quote = preg_replace('/[^A-Za-z0-9\-]/', '', strtolower($row['quote']));

            similar_text($submitted_quote, $database_quote, $percent);

            if ($percent > 80) {
                $response->existed = true;
                break;
            }
        }

        if ($response->existed) {

            // quote already exists
            $response->id = $row['id'];

            echo json_encode($response);
            exit;
        } else {

            // upload the new quote by prepared statement to prevent sql injection
            if ($response->loggedin) {

                // upload new quote with reference to user that uploaded it
                $stmt = $mysqli->prepare('INSERT INTO quotes (user_id, quote, author, insert_date) VALUES (?, ?, ?, NOW())');
                $stmt->bind_param('iss', $userid, $quote, $author);
            } else {

                // upload new quote without reference to user TODO notify user to register
                $stmt = $mysqli->prepare('INSERT INTO quotes (user_id, quote, author, insert_date) VALUES (1, ?, ?, NOW())');
                $stmt->bind_param('ss', $quote, $author);
            }

            $stmt->execute();
            $result = $stmt->affected_rows;

            if ($result === 1) {

                $response->id = mysqli_insert_id($mysqli);
        
                echo json_encode($response);
                exit;
            } else {
        
                echo json_encode($response);
                exit;
            }
        }
    } else {
        $response->error = "Quote text or author not set.";

        echo json_encode($response);
        exit;
    }
