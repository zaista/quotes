<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Guess a quote is a puzzle game in which you have to guess a hidden quote">
    <meta name="author" content="Jovan Ilic">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Guess a quote</title>
    <link rel="stylesheet" type="text/css" href="css/alert.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <link rel="icon" type="img/ico" href="favicon.ico">

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- For facebook crawler -->
    <meta property="og:url"          content="http://www.jovanilic.com/quotes/alert.html" />
    <meta property="og:site_name"    content="jovanilic.com" />
    <meta property="og:title"        content="Guess a quote" />
    <meta property="og:type"         content="website" />
    <meta property="og:description"  content="Try to guess a quote in this challeging little game!" />
</head>
<body>
    <div class="container-fluid">

        <!-- SESSION -->
        <input type="hidden" id="session-userid" value="<?php if (isset($_SESSION['userid'])) echo $_SESSION['userid']; ?>">
        <input type="hidden" id="session-username" value="<?php if (isset($_SESSION['username'])) echo $_SESSION['username']; ?>">
        <input type="hidden" id="session-useremail" value="<?php if (isset($_SESSION['useremail'])) echo $_SESSION['useremail']; ?>">
        <input type="hidden" id="session-quoteid" value="<?php if (isset($_POST['quoteid'])) echo $_POST['quoteid']; ?>">

        <!-- ALERT MESSAGE -->
        <div id="alert-div" class="col-md-4 col-md-offset-4 col-xs-12">
            <div class="alert alert-@type" role="alert">@message</div>
        </div>
    </div>
</body>
</html>
