<?php

    include 'config.php';
    $xml = get_config('config.xml');
    $mysqli = connect($xml);

    $json = new stdClass();

    // check if specific quote is requested
    if (isset($_GET['quote']) && $_GET['quote'] > 0) {

        // get the specified quote
        getQuote($_GET['quote'], $mysqli);
    } else {
        // if user is logged in get a random he didn't solve yet (or uploaded himselft)
        if (isset($_SESSION['userid'])) {
            $stmt = $mysqli->prepare("SELECT id FROM (SELECT quotes.id FROM quotes WHERE id NOT IN (
                            SELECT quotes.id FROM quotes JOIN solutions 
                            ON quotes.id = solutions.quote_id AND solutions.user_id = ?
                            UNION
                            SELECT quotes.id FROM quotes
                            WHERE quotes.user_id = ?)) AS result
                            ORDER BY RAND() LIMIT 1;");
            $stmt->bind_param('ii', $_SESSION['userid'], $_SESSION['userid']);
        } else {
            // just get any random quote
            $stmt = $mysqli->prepare("SELECT id FROM quotes ORDER BY RAND() LIMIT 1");
        }

        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();

            // get the selected quote id
            $id = $row['id'];
            getQuote($id, $mysqli);
        } else {
            $json->error = "No quote returned";
            echo json_encode($json);
            exit;
        }
    }

    function getQuote($id, $mysqli)
    {
        global $json;
        
        // protection from sql injection
        settype($id, 'integer');
        $sql = sprintf("SELECT * FROM quotes WHERE id=%d", $id);

        // perform the SQL query to get the quote by id
        if (!$result = $mysqli->query($sql)) {
            $json->error = "Code 4";
            echo json_encode($json);
            exit;
        }

        // check if any result is returned
        if ($result->num_rows === 0) {
            $json->error = "Invalid ID";
            echo json_encode($json);
            exit;
        }

        // get total number
        $row = $result->fetch_assoc();

        $json->id = $id;
        $json->quote = $row['quote'];
        $json->author = $row['author'];

        // return it as json object
        echo json_encode($json);

        $result->free();
        $mysqli->close();

        exit();
    }
