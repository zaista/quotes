<?php
    require_once '../vendor/autoload.php';
    include 'config.php';

    $session = new stdClass();

    if (isset($_SESSION['userid'])) {
        $session->userid = $_SESSION['userid'];
        $session->username = $_SESSION['username'];
        $session->useremail = $_SESSION['useremail'];
        $session->test = "test1";
    } elseif (isset($_GET['google-token'])) {
        $xml = get_config('config.xml');
        $mysqli = connect($xml);

        $client = new Google_Client(['client_id' => '569224685035-ha4looa0va5ji40cg0ee67pei8ivu933.apps.googleusercontent.com']);
        $payload = $client->verifyIdToken($_GET['google-token']);
        if ($payload) {
            $id;
            $username = $payload['given_name'];
            $email = $payload['email'];

            // match user or create a new one
            $stmt = $mysqli->prepare('SELECT id, email, active FROM users WHERE username = ?');
            $stmt->bind_param('s', $username);

            $stmt->execute();
            $result = $stmt->get_result();

            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();

                $id = $row['id'];
                $email = $row['email'];
            } else {
                
                // insert user into database
                $stmt = $mysqli->prepare('INSERT INTO users (username, password, email, insert_date, active) VALUES (?, "NULL", ?, NOW(), 1)');
                $stmt->bind_param('ss', $username, $email);

                $stmt->execute();
                $result = $stmt->affected_rows;

                if ($result === 1) {
                    $id = $stmt->insert_id;
                } else {
                    $session->error = 'Google user not logged in';
                }
            }

            // user logged in
            $_SESSION['userid'] = $id;
            $_SESSION['username'] = $username;
            $_SESSION['useremail'] = $email;

            $session->userid = $id;
            $session->username = $username;
            $session->useremail = $email;
            $session->test = "test2";
        } else {
            $session->error = 'Authentication problems.';
        }
    } else {
        $session->error = 'User not logged in';
    }

    $json = json_encode($session);
    echo $json;
