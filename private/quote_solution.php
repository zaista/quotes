<?php
    include 'config.php';
    $xml = get_config('config.xml');
    $mysqli = connect($xml);

    if (!isset($_SESSION['userid'])) {
        echo 'User not logged in';
        exit;
    } else {
        $userid = (int)$_SESSION['userid'];
    }

    if (!isset($_GET['quote'])) {
        echo 'Quote not submitted';
        exit;
    } else {
        $quoteid = (int)$_GET['quote'];

        // check if quote was already solved
        $stmt = $mysqli->prepare('SELECT * FROM solutions WHERE user_id = ? AND quote_id = ?');
        $stmt->bind_param('ii', $userid, $quoteid);

        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

        if ($result->num_rows > 0) {
            echo 'Quote already solved';
            exit;
        } else {

            // insert solution into database
            $stmt = $mysqli->prepare('INSERT INTO solutions (user_id, quote_id, insert_date) VALUES (?, ?, NOW())');
            $stmt->bind_param('ss', $userid, $quoteid);

            $stmt->execute();
            $result = $stmt->affected_rows;
            $stmt->close();

            if ($result === 1) {
                echo 'User soulution confirmed';
                exit;
            } else {
                echo "User soulution not confirmed";
                exit;
            }
        }
    }
