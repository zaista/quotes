<?php
    include 'private/config.php';
    $xml = get_config('private/config.xml');
    $mysqli = connect($xml);

    // Check if key is submitted
    if (isset($_GET['key'])) {
        $registration_key = mysqli_real_escape_string($mysqli, $_GET['key']);

        // check registration key
        $stmt = $mysqli->prepare('SELECT registrations.user_id, users.active FROM registrations, users WHERE registration_key = ? AND registrations.user_id = users.id');
        $stmt->bind_param('i', $registration_key);

        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $user_id = $row['user_id'];
            $active = $row['active'];

            if ($active === 0) {

                // activate the user
                $stmt = $mysqli->prepare('UPDATE users SET active = 1 WHERE id = ?');
                $stmt->bind_param('i', $user_id);

                $stmt->execute();
                $result = $stmt->affected_rows;

                if ($result === 1) {
                    alert('Email confirmed successfully.', 'success');
                    exit;
                } else {
                    alert('User could not be activated, click <a class="alert-link" href="quotes.html">here</a> to go back.', 'danger');
                    exit;
                }
            } else {
                alert('User is already activated, click <a class="alert-link" href="quotes.html?login=true">here</a> to login.', 'success');
                exit;
            }
        } else {
            alert('Registration key not found, click <a class="alert-link" href="quotes.html">here</a> to go back.', 'danger');
            exit;
        }
    } else {
        if (isset($_GET['username'])) {
            $stmt = $mysqli->prepare('SELECT id, email FROM users WHERE username = ?');
            $stmt->bind_param('s', $_GET['username']);

            $stmt->execute();
            $result = $stmt->get_result();

            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();

                // check if user is registered
                $user_id = $row['id'];
                $email = $row['email'];

                // generate registration key
                $registration_key = rand(1111111111, 9999999999);

                // insert registration key into database
                $stmt = $mysqli->prepare('INSERT INTO registrations (registration_key, user_id, insert_date) VALUES (?, ?, NOW())');
                $stmt->bind_param('ii', $registration_key, $user_id);

                $stmt->execute();
                $result = $stmt->affected_rows;

                if ($result === 1) {

                    //send verification email
                    $email_subject = "User activation";
                    $email_text = '
            Confirm your registration by following this link:
            https://quotes.jovanilic.com/activate.php?key=' . $registration_key . '

            Game of Quotes';

                    $email_headers = "From: quotes@jovanilic.com";

                    // use wordwrap() if lines are longer than 70 characters
                    $email_text = wordwrap($email_text, 70);

                    // send email
                    mail($email, $email_subject, $email_text, $email_headers);

                    alert('Verification email sent to: ' . $email . ', once you activate the user click <a class="alert-link" href="quotes.html?login=true">here</a> to login.', 'success');
                    exit;
                } else {
                    alert('Error sending activation email, click <a class="alert-link" href="quotes.html">here</a> to go back.', 'danger');
                    exit;
                }
            } else {
                alert('User <strong>' . $_GET['username'] . '</strong> not found, click <a class="alert-link" href="quotes.html">here</a> to go back.', 'danger');
                exit;
            }
        } else {
            alert('Invalid request, click <a class="alert-link" href="quotes.html">here</a> to go back.', 'danger');
            exit;
        }
    }

    // this will display the message in a nice way
    function alert($message, $type)
    {

        // load the file
        $output = file_get_contents('private/alert.php');

        // replace the values
        $output = str_replace("@message", $message, $output);
        $output = str_replace("@type", $type, $output);

        echo $output;
    }
